;; fruity.el
;; 
;; fruity is an Emacs colorscheme created by Taurus Olson.
;; with the Emacs color-theme creator 
;; (http://alexpogosyan.com/color-theme-creator/)
;; 
;; Date: Nov 11, 2011

(defun fruity ()
  (interactive)
  (color-theme-install
   '(fruity
      ((background-color . "#050505")
      (background-mode . light)
      (border-color . "#303030")
      (cursor-color . "#d26e0f")
      (foreground-color . "#ffffff")
      (mouse-color . "black"))
     (fringe ((t (:background "#303030"))))
     (mode-line ((t (:foreground "#000000" :background "#1adf11"))))
     (region ((t (:background "#191715"))))
     (font-lock-builtin-face ((t (:foreground "#eaa21a"))))
     (font-lock-comment-face ((t (:foreground "#737373"))))
     (font-lock-function-name-face ((t (:foreground "#8f3ad4"))))
     (font-lock-keyword-face ((t (:foreground "#4eadf9"))))
     (font-lock-string-face ((t (:foreground "#e1d109"))))
     (font-lock-type-face ((t (:foreground"#ed3b1d"))))
     (font-lock-variable-name-face ((t (:foreground "#f9951f"))))
     (minibuffer-prompt ((t (:foreground "#a513d8" :bold t))))
     (font-lock-warning-face ((t (:foreground "Red" :bold t))))
     )))
(provide 'fruity)