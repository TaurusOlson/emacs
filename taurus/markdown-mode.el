(require 'markdown-mode)

(autoload 'markdown-mode "markdonw-mode.el"
  "Major mode for editing Markdown files" t)

(setq auto-mode-alist
      (cons '("\\.md" . markdown-mode) auto-mode-alist)
      ;; (cons '("\\.markdown" . markdown-mode) auto-mode-alist)
      ;; (cons '("\\.mkd" . markdown-mode) auto-mode-alist)
      )