(defvar *github-root* "~/Github")
(defvar *python-root* "~/Programming/python")
(defvar *ruby-root*   "~/Programming/ruby")
(defvar *lisp-root*   "~/Programming/Lisp")
(defconst *zsh*       "~/.zsh/")
(defconst *p5*        "~/Documents/Processing/") 
(defconst *emacs.d*   "~/.emacs.d/")

(defun bookmark-ido-find-file (dir)
  (interactive)
  (ido-find-file-in-dir dir))

(defun github-ido-find-file ()
  (interactive)
  (ido-find-file-in-dir *github-root*))

(defun zsh-ido-find-file ()
  (interactive)
  (ido-find-file-in-dir *zsh*))

(defun p5-ido-find-file ()
  (interactive)
  (ido-find-file-in-dir *p5*))

(defun taurus-ido-find-config ()
  "Find emacs config files (by defunkt)"
  (interactive)
  (find-file
   (concat "~/.emacs.d/taurus/"
	   (ido-completing-read "Config file: "
                                (directory-files "~/.emacs.d/taurus/" nil ".el$")))))

(defun taurus-goto-config ()
  (interactive)
  (find-file "~/.emacs"))

