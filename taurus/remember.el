(require 'remember)

(setq remember-data-file "~/Dropbox/org/notes.org")
(global-set-key (kbd "C-c r") 'remember)
(setq remember-annotation-functions '(org-remember-annotation))
(setq remember-handler-functions '(org-remember-handler))
(add-hook 'remember-mode-hook 'org-remember-apply-template)

(eval-after-load 'remember
  '(progn
    (add-to-list 'remember-annotation-functions 'buffer-name t)))

(defun wicked/remember-line-numbers-and-file-names ()
 "Return FILENAME line NUMBER."
 (save-restriction
  (widen)
   (format " %s line %d"
    (or (buffer-file-name) (buffer-name))
    (line-number-at-pos))))
(eval-after-load 'remember
  '(progn
     (add-to-list 'remember-annotation-functions
                  'wicked/remember-line-numbers-and-file-names)))

