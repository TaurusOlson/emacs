;; COLOR-THEMES ---------------------------------------------------------------
(add-to-list 'load-path "~/.emacs.d/themes")

(require 'color-theme)
(setq color-theme-is-global t)

(require 'color-theme-julie)
(require 'folio)
(require 'zenburn)
(require 'gunmetal)
(require 'color-theme-twilight)
(require 'color-theme-bluebulator)
(color-theme-julie)
