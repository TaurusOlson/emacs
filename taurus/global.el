;; GLOBAL OPTIONS -------------------------------------------------------------
(require 'paren)

; Mac keyboards
(setq ns-command-modifier 'meta)

(setq mac-option-modifier nil
      mac-command-modifier 'meta
      x-select-enable-clipboard)

(setq mac-pass-command-to-system nil) ;avoid hiding with M-h
(setq make-backup-file nil) ; don't make backup files

(setq default-frame-alist (append (list 
  '(width  . 79)  ; Width set to 79 characters 
  '(height . 40)) ; Height set to 40 lines 
  default-frame-alist)) 

;; display column number
(column-number-mode 1)

;; Remove menubar and toolbar
; (menu-bar-mode nil)
; (tool-bar-mode nil)

;; No ring-bell
(setq ring-bell nil)

(setq transient-mark-mode        t)   ; make marked region highlighted
(setq inhibit-startup-message    t)   ; Don't want any startup message 
(setq inhibit-splash-screen      t)   ; Don't display the splash screen
(setq make-backup-files        nil)   ; Don't want any backup files 
(setq auto-save-list-file-name nil)   ; Don't want any .saves files 
(setq auto-save-default        nil)   ; Don't want any auto saving 
(setq auto-save-list-file-prefix nil) ; Don't want any auto-save-list
(setq search-highlight           t)   ; Highlight search object 
(setq query-replace-highlight    t)   ; Highlight query object 
(setq mouse-sel-retain-highlight t)   ; Keep mouse high-lightening 

;; y or n instead of yes or no
(fset 'yes-or-no-p 'y-or-n-p)

;; Display time
(display-time)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)

;; Activer la coloration syntaxique
(global-font-lock-mode t)

(show-paren-mode)
