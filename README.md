# DESCRIPTION

This is my emacs configuration. I'm still new to Emacs and I've been a Vim user
for 3 years so this may seem a bit simple and clumsy to long time Emacs users.

# CONFIGURATION

The current configuration contains:

- [deft][deft-mode] 
- [remember][remember-mode]
- [yasnippet][yasnippet-mode]
- [processing-emacs][processing-mode]
- [python-mode][python-mode]

# THANKS

defunkt for its [emacs configuration ][defunktemacs] by which I was inspired.


[defunktemacs]: https://github.com/defunkt/emacs
[deft-mode]: http://jblevins.org/projects/deft/
[remember-mode]: https://gna.org/p/remember-el
[yasnippet-mode]: http://code.google.com/p/yasnippet/
[processing-mode]: http://github.com/omouse/processing-emacs
[python-mode]: https://launchpad.net/python-mode
